<?php

namespace App\Service;

use App\Repository\ItemsRepository;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CartService{
    protected $session;
    protected $itemsRepository;
    public function __construct(SessionInterface $session, ItemsRepository $itemsRepository)
    {
        $this->session= $session;
        $this->itemsRepository = $itemsRepository;
    }

    public function add(int $id){

        $panier = $this->session->get('panier', []);

        if(!empty($panier[$id])){
            $panier[$id]++;
        } else {

            $panier[$id]= 1;
        }
        

        $this->session->set('panier', $panier);
    }


    public function remove(int $id){
        $panier = $this->session->get('panier',[]);

        if(!empty($panier[$id])){
            unset($panier[$id]);
        }
        $this->session->set('panier',$panier);
    }

    public function getFullCart() : array {
        $panier = $this->session->get('panier',[]);

        foreach($panier as $id => $quantity){
            $panierWithData[] = [
                'items' => $this->itemsRepository->find($id),
                'quantity' => $quantity
                
            ];
            
        }
       
        return $panierWithData;

    }

    public function getTotal() : float {

        $total = 0;
        

        foreach($this->getFullCart() as $item){
            
            $total += $item['items']->getPrice() * $item['quantity'];
        }
    return $total;
    }

    public function getTotalQ() : float {

        $totalQ = 0;
        

        foreach($this->getFullCart() as $item){
            
            $totalQ +=  $item['quantity'];
        }

        $this->session->set('cartData', $this->getFullCart());
    return $totalQ;
    }
    


}