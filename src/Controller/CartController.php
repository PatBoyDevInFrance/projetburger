<?php

namespace App\Controller;

use App\Service\CartService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    /**
     * @Route("/mon-panier", name="cart_index")
     */
    public function index(CartService $carteService): Response
    {        
        return $this->render('cart/index.html.twig',[
           'items' => $carteService->getFullCart(),
           'total' => $carteService->getTotal(),
           'totalQ' => $carteService->getTotalQ()
        ]);
    }

    /**
     * @Route("/panier/add/{id}", name="cart_add")
     */
    public function add($id, CartService $cartService  ){
        $cartService->add($id);
        return $this->redirectToRoute('cart_index');
    }

    
    /**
     * @Route("/panier/remove/{id}", name="cart_remove")
     */
    public function remove($id,  CartService $cartService ){
        $cartService->remove($id);

        return $this->redirectToRoute('carte_index');
    }
}
