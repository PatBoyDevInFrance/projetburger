<?php

namespace App\Controller;

use App\Repository\ItemsRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BurgerController extends AbstractController
{
    /**
     * @Route("/burger", name="burger")
     */
    public function index(ItemsRepository $itemsRepository, PaginatorInterface $paginatorInterface, Request $request): Response
    {   

        $carte = $paginatorInterface->paginate(
        $itemsRepository->findAllWithPaginator(), /* query NOT result */
        $request->query->getInt('page', 1), /*page number*/
        6 /*limit per page*/
    );

        return $this->render('burger/index.html.twig', [
            'cartes' => $carte
        ]);
    }

    
    
}
