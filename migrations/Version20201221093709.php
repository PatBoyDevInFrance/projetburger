<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201221093709 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE categories ADD items_id INT DEFAULT NULL, CHANGE name name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE categories ADD CONSTRAINT FK_3AF346686BB0AE84 FOREIGN KEY (items_id) REFERENCES items (id)');
        $this->addSql('CREATE INDEX IDX_3AF346686BB0AE84 ON categories (items_id)');
        $this->addSql('ALTER TABLE items DROP FOREIGN KEY items_ibfk_1');
        $this->addSql('DROP INDEX category ON items');
        $this->addSql('ALTER TABLE items DROP category, CHANGE name name VARCHAR(255) NOT NULL, CHANGE description description LONGTEXT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE categories DROP FOREIGN KEY FK_3AF346686BB0AE84');
        $this->addSql('DROP INDEX IDX_3AF346686BB0AE84 ON categories');
        $this->addSql('ALTER TABLE categories DROP items_id, CHANGE name name VARCHAR(100) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`');
        $this->addSql('ALTER TABLE items ADD category INT NOT NULL, CHANGE name name VARCHAR(100) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, CHANGE description description VARCHAR(255) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`');
        $this->addSql('ALTER TABLE items ADD CONSTRAINT items_ibfk_1 FOREIGN KEY (category) REFERENCES categories (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('CREATE INDEX category ON items (category)');
    }
}
